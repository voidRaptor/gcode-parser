from machineclient import MachineClient
import sys

# TODO: ignore parts that are in ()
# TODO: if ( in line -> search for )
#           if not found -> search in following lines
# TODO: read to single string to more easily cut comments out?
# TODO: __lines paired with prognum in dict? -> allows multiple progs per file
# TODO: cmd sanity check on fileread
# TODO: file reading into separate function
# TODO: save states, e.g. abs/incr/planeselect
# TODO: enums for const strings
# TODO: methods that need diff args into diff dict



class Interpreter:
    """Interpreter for gcode programs from a file

    Params:
        filename (str): path to gcode file

    """
    
    client = MachineClient()

    __lines = []
    __programNumber = None 
    __programmingMode = None  # ABSOLUTE / INCREMENTAL / None(unset)
    __movementMode = None  # LINEAR / None
    __plane = None  # XY / XZ / YZ
    __using_mm = True
    __x_acc = 0  # accumulative
    __y_acc = 0  # accumulative
    __z_acc = 0  # accumulative


    def __init__(self, filename):
    
        with open(filename, "r") as gcodefile:

            for line in gcodefile:
                line = line.rstrip("\n") 
            
                # add relevant lines only 
                if not (line.isspace() or line == "") and \
                    line[0] != "(" and \
                    line[0] != "%":

                    parsed = self.__parseLine(line)
                    self.__lines.append(parsed)

        if self.__programNumber != None:
            self.__lines.pop(0)
            

    def print(self):
        print("Program: ", self.__programNumber)

        for line in self.__lines:
            print(line)


    def execute(self):
        """Execute the program stored in Interpreter

        Returns:
            True: if progman is executed successfully
            False: if incorrect command is found

        Calls MachineClients functions directly, if command is a value (e.g. 
        X-12.000), and via handler function, if given command is a number for 
        a gcode command (e.g. G00).

        G & M = commnds with code numbers 
        T = tool select
        F = feed rate
        S = spindle rotation speed
        X = x coord
        Y = y coord
        Z = z coord

        """
        for line in self.__lines:
            for cmd in line:

                # retrieve action for cmd from const dict    
                if cmd in self.CMD_DICT:
                    begin = line.index(cmd)
                    self.CMD_DICT[cmd](self, line[begin:])

                elif cmd[0] == "T":
                    self.client.change_tool( cmd[1:] )

                elif cmd[0] == "F":
                    value = float(cmd[1:])

                    if not self.__using_mm:
                        value = self.__in_to_mm(value)

                    self.client.set_feed_rate(value)

                elif cmd[0] == "S":
                    self.client.set_spindle_speed( float(cmd[1:]) )

                elif cmd[0] != "X" and cmd[0] != "Y" and cmd[0] != "Z":
                    print( "Error: command {} not found".format(cmd) )
                    return False

        return True


    def __parseLine(self, line):
        """Parse single gcode line
        
        Params:
            line (str): line to be parsed

        Returns:
            list of commands

        """
        args = line.split(" ")

        if args[0][0] == "O":
            self.__programNumber = args[0]
            return []
        
        if "N" in args[0]:
            args.pop(0)

        return args

    
    def __in_to_mm(self, value):
        return value * 25.4


    def __move(self, args):
        """Move to given coordinates

        If coordinate is not set, will keep previous value. Also takes 
        incremental and absolute programming modes into account.

        Params:
            args (list): list of arguments gcode can take, unnecessary will be
                         filtered out

        """
        # TODO: add mode checks (plane etc)
        # TODO: plane doesnt matter since 3rd coord must be somehow set?
        # TODO: refactor if else conds 
        
        x = None
        y = None
        z = None

        for cmd in args:
            value = float(cmd[1:]) 
            
            if not self.__using_mm:
                value = self.__in_to_mm(value)
            
            if "X" in cmd:
                x = value     

            elif "Y" in cmd:
                y = value    
            
            elif "Z" in cmd:
                z = value

        if self.__programmingMode == "INCREMENTAL":
            
            if x != None:
                self.__x_acc += x
            
            if y != None:
                self.__y_acc += y
           
            if z != None:
                self.__z_acc += z
                
            self.client.move_x(self.__x_acc)
            self.client.move_y(self.__y_acc)
            self.client.move_z(self.__z_acc)


        # if not set, coordinate must stay the same
        else:

            if x != None:
                self.client.move_x(x)
            
            if y != None:
                self.client.move_y(y)
           
            if z != None:
                self.client.move_z(z)
        
    
    ### gcode dict callables ###

    def __rapid_positioning(self, args):
        self.client.set_feed_rate(sys.float_info.max)
        self.__move(args)


    def __linear_interpolation(self, args):
        self.__move(args)


    def __select_XY_plane(self, args):
        self.__plane = "XY"
        print("xy")


    def __set_unit_mm(self, args):
        self.__unit_mm = True
        print("mm")


    def __return_home(self, args):
        self.client.home()


    def __tool_rad_compensation_off(self, args):
        print("rad compensation off")


    def __tool_length_offset_compensation_off(self, args):
        print("length offset compensation off")


    def __set_work_coord_sys(self, args):
        print("WCS")


    def __cancel_canned_cycle(self, args):
        print("cancel canned")


    def __abs_or_fixed_cycle(self, args):

        if self.__programmingMode == "ABSOLUTE":
            self.__fixed_cycle_z()

        else:
            self.__set_abs_programming(args)


    def __set_abs_programming(self, args):
        self.__programmingMode = "ABSOLUTE"
        self.__x_acc = 0
        self.__y_acc = 0
        self.__z_acc = 0
        print("absolute")


    def __set_incr_programming(self, args):
        self.__programmingMode = "INCREMENTAL"
        print("incremental")


    def __fixed_cycle_x(self, args):
        print("fixed cycle (x)")


    def __fixed_cycle_z(self, args):
        print("fixed cycle (z)")
    
    
    def __spindle_on(self, args):
        print("spindle on")


    def __spindle_stop(self, args):
        print("spindle stop")


    def __automatic_tool_change(self, args):
        print("auto tool change")


    def __coolant_on_mist(self, args):
        self.client.coolant_on()


    def __coolant_on_flood(self, args):
        self.client.coolant_on()


    def __coolant_off(self, args):
        self.client.coolant_off

    
    def __end_of_program(self, args):
        print("EOP")



    # dictionary for mapping command codes and callables
    CMD_DICT = {

        "G00" : __rapid_positioning,
        "G01" : __linear_interpolation,
        "G17" : __select_XY_plane,
        "G21" : __set_unit_mm,
        "G28" : __return_home,
        "G40" : __tool_rad_compensation_off,
        "G49" : __tool_length_offset_compensation_off,
        "G54" : __set_work_coord_sys,
        "G80" : __cancel_canned_cycle,
        "G90" : __abs_or_fixed_cycle,
        "G91" : __set_incr_programming,
        "G94" : __fixed_cycle_x,
        
        "M03" : __spindle_on,
        "M05" : __spindle_stop,
        "M06" : __automatic_tool_change,
        "M07" : __coolant_on_mist,
        "M08" : __coolant_on_flood,
        "M09" : __coolant_off,
        "M30" : __end_of_program

    } 


