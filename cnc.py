import interpreter
import sys


def main():
    filename = sys.argv[1]
    interp = interpreter.Interpreter(filename)

    if not interp.execute():
        print("Stopped executing")



if __name__ == "__main__":
    main()

